import json
from lxml import html
import requests
import lxml.html as lh
import urllib3
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

session_requests = requests.session()
login_url = "https://www.strava.com/login"
result = session_requests.get(login_url)
tree = html.fromstring(result.text)
authenticity_token = list(set(tree.xpath("//input[@name='authenticity_token']/@value")))[0]
payload = {'email': 'nelson_mkrtchyan@edu.aua.am', 'password': 'Capstone2020', 'authenticity_token': authenticity_token}
result = session_requests.post(
    "https://www.strava.com/session",
    data=payload,
    headers=dict(referer=login_url)
)

url = "https://www.strava.com/segments/19182908?filter=overall&per_page=100"

response = session_requests.get(url)
tree = html.fromstring(response.content)
soup = BeautifulSoup(response.content, 'html.parser')

def getHrefs():
    hrefs = []
    container = tree.xpath("//div[@id='results']")[0]
    links = container.xpath('.//a/@href')
    for link in links:
        hrefs.append(link)
    return hrefs


def filt(hrefs):
    filteredElems = []
    for el in hrefs:
        if "athletes" in el:
            filteredElems.append(el)
    return filteredElems


hrefs = getHrefs()
filtered = filt(hrefs)


# def getMins():
#     data = []
#     for path in filtered:
#         url = 'https://www.strava.com/' + path
#         res = session_requests.get(url)
#         soup = BeautifulSoup(res.content, 'html.parser')
#         try:
#             mins = soup.find("ul", {"id":"totals"}).findChildren()[3].text
#             mins  = float(mins.split('h')[0]) * 60 + float(mins.split('h')[1].split('m')[0])
#             data.append([1,mins])
#         except Exception:
#             pass
#     return data


def getCols():
    # driver = webdriver.Chrome()
    data = []
    for path in filtered:
        url = 'https://www.strava.com/' + path + '#interval_type?chart_type=hours&interval_type=week&interval=202009&year_offset=0&_=1584265819756'
        # params={'chart_type':'hours', 'interval_type':'week', 'interval':'202009', 'year_offset':'0', '_':'1583140406668'}
        headers = dict(XRequestedWith='XMLHttpRequest')
        res = session_requests.get(url, headers=headers)
        soup = BeautifulSoup(res.content, 'html.parser')
        try:
            cont = soup.find("div", {"id": "interval-graph-columns"}).findChildren("ul")[1]
            columns = cont.findChildren('li')
            konaColumn = columns.index(cont.findChildren("li", {"id": "interval-201941"})[0])
            scale = soup.find("div", {"id": "interval-graph-columns"}).findChildren("ul")[0]
            print(scale.findChildren('li')[2].text)
            ratio = float(scale.findChildren('li')[2].text) / float(
                scale.findChildren('li')[2]['style'].split(':')[1].split('px')[0])
            id = path.split('/')[-1]
            inner_data = [id]
            for i in range(konaColumn - 20, konaColumn):
                height = columns[i].findChildren('a')[0].findChildren('div')[0]['style'].split(':')[1].split('px')[0]
                hours = round(int(height) * ratio, 2)
                inner_data.append(hours)
            data.append(inner_data)

            # driver.get(url)
            # ActionChains(webdriver.Chrome()).move_to_element(webdriver.Chrome().find_element(By.ID,"interval-graph-columns").find_elements(By.XPATH, '//ul')[1].find_elements(By.XPATH, '//li')[0]).click(webdriver.Chrome().find_elements(By.XPATH, '//a')[0]).perform()
            # print(driver.find_element(By.ID,"interval-graph-columns"))
        except Exception:
            pass
    return data


data = getCols()
print(data)
headers = ['Week' + str(i) for i in range(1, len(data[0]))]
headers.insert(0, 'id')
data_pd = pd.DataFrame(data, columns=headers)

data_pd.to_csv('week_time_100_Yerevan.csv')

print(data)
