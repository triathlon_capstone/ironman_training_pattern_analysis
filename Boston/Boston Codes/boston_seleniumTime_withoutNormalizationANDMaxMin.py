from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import time
import numpy as np
import pandas as pd

# driver = webdriver.Chrome()

driver = webdriver.Chrome(executable_path=r'C:\webdrivers\chromedriver.exe')  # for Nelson's laptop

login_url = "https://www.strava.com/login"
# table_url = "https://www.strava.com/segments/19182908?filter=overall&per_page=100" #  used lower


driver.get(login_url)
driver.find_element_by_id("email").send_keys("nelsonwaterpolo@gmail.com")
driver.find_element_by_id("password").send_keys("Nelson99")
driver.find_element_by_id("login-button").click()
time.sleep(2)
request_cookies_browser = driver.get_cookies()

hrefs = []


def getHrefs():
    container = driver.find_element_by_id("results-table-container")
    links = container.find_elements_by_tag_name('a')
    for link in links:
        hrefs.append(link.get_attribute('href'))
    return hrefs


def filt(hrefs):
    filteredElems = []
    for el in hrefs:
        if "athletes" in el:
            filteredElems.append(el)
    return filteredElems


# numbers = [1, 2, 3]  # number of pages

# numbers = [4, 5, 6, 7]  # number of pages

# numbers = [8, 9, 10, 11]  # number of pages

# numbers = [12, 13, 14, 15, 16]  # number of pages

# numbers = [17, 18, 19, 20, 21, 22, 23, 24, 25]  # number of pages

# numbers = [26, 27, 28, 29, 30, 31, 32, 33, 34, 35]  # number of pages

# numbers = [36, 37, 38, 39, 40, 41, 42, 43, 44, 45]  # number of pages

# numbers = [46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57]  # number of pages

# numbers = [56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68]  # number of pages

# numbers = [69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82]  # number of pages

# numbers = [83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98]  # number of pages

numbers = [99, 100, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115]  # number of pages


for number in numbers:
    table_url = "https://www.strava.com/running_races/2494?filter=overall&page=" + str(number)
    print(table_url)

    driver.get(table_url)
    time.sleep(2)
    hrefs = getHrefs()

filtered = filt(hrefs)


def getCols():
    data = []
    for path in filtered:
        athlete_url = path + '#interval_type?chart_type=hours&interval_type=week&interval=202009&year_offset=0'
        driver.get(athlete_url)
        time.sleep(5)

        try:
            elem = driver.find_element_by_id("interval-graph-columns")
            elemForRation = elem.find_elements_by_class_name("y-axis")[0].find_elements_by_class_name("label")[2]
            scaleTime = elemForRation.text
            scalePixel = elemForRation.get_attribute("style").split(':')[1].split('px')[0]
            ratio = float(scaleTime) / float(scalePixel)
            cont = elem.find_elements_by_tag_name("ul")[1]
            columns = elem.find_elements_by_tag_name("li")
            konaColumn = columns.index(cont.find_element_by_id("interval-201941"))
            id = path.split('/')[-1]
            inner_data = [id]
            for i in range(konaColumn - 20, konaColumn):
                height = columns[i].find_elements_by_tag_name('a')[0].find_elements_by_tag_name('div')[0].get_attribute(
                    "style").split(':')[1].split('px')[0]
                hours = round(int(height) * ratio, 2)
                inner_data.append(hours)
            data.append(inner_data)

        except Exception:
            pass
    return data


data = getCols()
print(data)
headers = ['Week' + str(i) for i in range(1, len(data[0]))]
headers.insert(0, 'id')
data_pd = pd.DataFrame(data, columns=headers)

data_pd.to_csv('week_time_(99-115_pages)_Boston_seleniumTime.csv')
