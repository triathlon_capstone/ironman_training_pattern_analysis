from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import time
import numpy as np
import pandas as pd

# driver = webdriver.Chrome() # Driver for some computers
driver = webdriver.Chrome(executable_path=r'C:\webdrivers\chromedriver.exe')  # Driver for other computers

login_url = "https://www.strava.com/login"

driver.get(login_url)

driver.find_element_by_id("email").send_keys("satenik.mnatsakanyan@gmail.com")
driver.find_element_by_id("password").send_keys("AUA_Coach2All")

driver.find_element_by_id("login-button").click()
time.sleep(2)
request_cookies_browser = driver.get_cookies()

hrefsAndDates = []
arrayOfRanks = []

def getHrefsAndDates():
    container = driver.find_element_by_id("results")
    tbody = container.find_elements_by_tag_name("tbody")[0]
    trs = tbody.find_elements_by_tag_name('tr')

    for tr in trs:
        arrRanksAndLinks = []
        td = tr.find_elements_by_tag_name('td')[0]
        links = tr.find_elements_by_tag_name('a')
        trArr = []
        for link in links:
            href = link.get_attribute('href')
            if "athletes" in href:
                trArr.append(href)
                arrRanksAndLinks.append(td.text if td.text else 1)
                arrRanksAndLinks.append(href.split('/')[-1])
                arrayOfRanks.append(arrRanksAndLinks)
            if "pros" in href:
                trArr.append(href)
                arrRanksAndLinks.append(td.text if td.text else 1)
                arrRanksAndLinks.append(href.split('/')[-1])
                arrayOfRanks.append(arrRanksAndLinks)
            elif "segment_efforts" in href:
                trArr.append(link.text)
        hrefsAndDates.append(trArr)
    return hrefsAndDates

max_page_number = 36  # This range depends on the participants number in the list
numbers = np.array(range(1, max_page_number))

age_group = ['0_19', '20_24', '25_34', '35_44', '45_54', '55_64', '65_69', '70_74', '75_plus']

gender = ['M', 'F']

for number in numbers:
    table_url = "https://www.strava.com/segments/13650960?filter=overall&per_page=100&page=" + str(
        number) + "&age_group=" + age_group[8] + "&filter=age_group&gender=" + gender[0]
    print(table_url)

    driver.get(table_url)
    time.sleep(2)
    hrefsAndDates = getHrefsAndDates()


def getCols():
    currentYear = "2019"
    finalData = []
    for index, hrefAndDate in enumerate(hrefsAndDates):
        path = hrefAndDate[0]
        date = hrefAndDate[1][-4:]

        try:
            athlete_url = path + '#interval_type?chart_type=hours&interval_type=week&interval=202009&year_offset=0'
            driver.get(athlete_url)
            time.sleep(10)
        except:
            pass

        if date != currentYear:
            try:
                dateSection = driver.find_element_by_id("interval-date-range")
                dateSection.click()
                time.sleep(2)
                options = dateSection.find_elements_by_tag_name('li')
                for li in options:
                    checkDate = li.text.split('-')[0][-5:-1]
                    if checkDate == date:
                        li.click()
                        time.sleep(10)
                        timeHistogram = \
                            driver.find_element_by_id("interval-graph-controls").find_elements_by_tag_name("ul")[
                                0].find_elements_by_tag_name("li")[0].find_elements_by_tag_name("ul")[
                                0].find_elements_by_tag_name("li")[0]
                        timeHistogram.click()
                        time.sleep(10)
                        break
            except:
                pass
        try:
            elem = driver.find_element_by_id("interval-graph-columns")
            elemForRation = elem.find_elements_by_class_name("y-axis")[0].find_elements_by_class_name("label")[2]
            scaleTime = elemForRation.text
            scalePixel = elemForRation.get_attribute("style").split(':')[1].split('px')[0]
            ratio = float(scaleTime) / float(scalePixel)
            cont = elem.find_elements_by_tag_name("ul")[1]
            columns = cont.find_elements_by_tag_name("li")
            konaColumn = columns.index(cont.find_element_by_id("interval-{}41".format(date)))
            id = path.split('/')[-1]
            inner_data = [arrayOfRanks[index][0], id]
            for i in range(konaColumn - 20, konaColumn):
                if columns[i].find_elements_by_tag_name('a'):
                    height = \
                        columns[i].find_elements_by_tag_name('a')[0].find_elements_by_tag_name('div')[0].get_attribute(
                            "style").split(':')[1].split('px')[0]
                    hours = round(int(height) * ratio, 2)
                    inner_data.append(hours)
                else:
                    hours = 0
                    inner_data.append(hours)
            finalData.append(inner_data)

        except Exception:
            pass
    return finalData


data = getCols()
length = len(data[0]) - 1
headers = ['Week ' + str(i) for i in range(1, length)]
headers.insert(0, 'Rank')
headers.insert(1, 'id')
data_pd = pd.DataFrame(data, columns=headers)

data_pd.to_csv('./New_DataSets/Kona_Male_75_plus.csv')


