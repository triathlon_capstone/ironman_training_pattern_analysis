import numpy as np
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from sklearn.preprocessing import Normalizer

initialData = pd.read_csv('week_time_All_Kona.csv')  # load data set

x = np.array(initialData)

min = 7
max1 = 40
max2 = 50
countForMaxFirst = 0
countForMaxSecond = 0
maxCountForFirst = 2
maxCountForSecond = 1

dataWithHolds = []

for row in range(0, len(x)):
    inner_data = [int(x[row, 1])]  # adding id to the row
    for col in range(2, 22):
        hours = x[row, col]
        if int(hours) in range(min, max2):
            if int(hours) > max1:
                countForMaxSecond = countForMaxSecond + 1
                if countForMaxSecond <= maxCountForSecond:
                    inner_data.append(hours)
                else:
                    inner_data.append("hold")
            elif int(hours) == max1:
                countForMaxFirst = countForMaxFirst + 1
                if countForMaxFirst <= maxCountForFirst:
                    inner_data.append(hours)
                else:
                    inner_data.append("hold")
            else:
                inner_data.append(hours)
        else:
            inner_data.append("hold")
    dataWithHolds.append(inner_data)

# This is a function to calculate and insert avarage weekly training values instead of outliers values

def fillWithAverage(arr):
    for column in range(0, len(arr[0])):
        averageForWeek = 0.0
        sumForWeek = 0.0
        for row in range(0, len(arr)):
            if (not isinstance(arr[row][column], str)):
                sumForWeek = sumForWeek + arr[row][column]
        averageForWeek = round(float(sumForWeek / len(arr)), 2)
        if averageForWeek == 0.0:
            print(sumForWeek)
        for i in range(0, len(arr)):
            if arr[i][column] == 'hold':
                arr[i][column] = averageForWeek
    return arr


data = fillWithAverage(dataWithHolds)

normalized_data = Normalizer().fit(data)

headers = ['Week' + str(i) for i in range(1, len(data[0]))]
headers.insert(0, 'id')
data_pd = pd.DataFrame(data, columns=headers)

data_pd.to_csv('kona_MinMax_1015_Normalized_New_New.csv')
