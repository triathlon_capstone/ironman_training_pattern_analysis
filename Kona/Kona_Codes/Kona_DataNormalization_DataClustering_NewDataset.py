import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tslearn.clustering import TimeSeriesKMeans
from tslearn.datasets import CachedDatasets
from tslearn.preprocessing import TimeSeriesScalerMeanVariance, \
    TimeSeriesResampler
from tslearn.utils import to_time_series_dataset

from tslearn.metrics import cdist_dtw
from tslearn.clustering import silhouette_score

import csv
import matplotlib.pyplot as plt
import copy

###############################################################
# Normalizing then Clustering
#####################################################

# adding data
number_of_clusters = 2

# metricOfClustering = "dtw"
metricOfClustering = "euclidean"
title = 'Kona_Female_0_34_Smaller_Median(10.0)'

with open('New_DataSets/Medians/' + title +'.csv', 'r') as dest_f:
    data_iter = csv.reader(dest_f,
                           # delimiter = delimiter,
                           quotechar='"')
    full_data = np.array([np.array(i_data[:25]) for i_data in data_iter])
    full_data = full_data[1:]
copyOfFull_data = copy.deepcopy(full_data)
datawithId = full_data[:, 1:23].astype(float)
data = datawithId[:, 2:23]
# print(len(datawithId))

X = data[:].astype(float)
sz = X.shape[1]
############################################################################
# Transfoming data

arr_of_ind = []
for i, row in enumerate(X):
    if (sum(row < 7) + sum(row > 40)) > 10:
        arr_of_ind.append(i)

### for ID,Median, Mean CSV
copyOfFull_data = np.delete(copyOfFull_data, arr_of_ind, axis=0)
###

X = np.delete(X, arr_of_ind, axis=0)
datawithId = np.delete(datawithId, arr_of_ind, axis=0)


############################################################################
# Rolling avarage

def RollingAvarage2(array):
    copyOfArray = copy.deepcopy(array)
    for i, row in enumerate(array):
        for j, col in enumerate(row):
            if j == 0:
                array[i][j] = (copyOfArray[i][j] + copyOfArray[i][j + 1]) / 2
            elif j == len(row) - 1:
                array[i][j] = (copyOfArray[i][j - 1] + copyOfArray[i][j]) / 2
            else:
                array[i][j] = (copyOfArray[i][j - 1] + copyOfArray[i][j] + copyOfArray[i][j + 1]) / 3

    return array


def getNorm(arr):
    for row in range(0, len(arr)):
        maximumVal = 0.0
        minimumVal = 0.0
        for column in range(0, len(arr[0])):

            if column == 1:
                minimumVal = arr[row][column]
            if float(arr[row][column]) >= maximumVal:
                maximumVal = arr[row][column]
            if float(arr[row][column]) < minimumVal:
                minimumVal = arr[row][column]

        for column in range(0, len(arr[0])):
            arr[row][column] = (arr[row][column] - minimumVal) / (maximumVal - minimumVal)
            arr[row][column] = round(arr[row][column], 2)

    return arr


# print(X[0:3])
X = RollingAvarage2(X)
# print(X[0:3])
# print("datawihId")
# print(datawithId[0:3])
datawithId[:, 2:] = RollingAvarage2(datawithId[:, 2:])
# print(datawithId[0:3])
X = getNorm(X)

datawithId[:, 2:] = getNorm(datawithId[:, 2:])

############################################################################


############################################################################
# Clustering

X_bis = to_time_series_dataset(X)

a = TimeSeriesKMeans(n_clusters=number_of_clusters, max_iter=5, metric=metricOfClustering, random_state=0)
b = a.fit_predict(X_bis)

############################################################################
# extracting ID array from clustered data

arrayofId = []
clusteredArray = []


def check(test, array):
    result = any(np.array_equal(x, test) for x in array)
    return result


def extractIDArray(number_of_clusters, datawithId, X):
    for i in range(number_of_clusters):
        Id = []
        cluster = []
        for rowInX in datawithId:
            # print(rowInX[0].astype(int),rowInX[1:].astype(float))
            if check(rowInX[1:].astype(float), X[np.where(b == i)]):
                # print("mtnuma ifi mej")
                Id.append(str(rowInX[0].astype(int)))
                cluster.append(rowInX[1:])
        arrayofId.insert(i, Id)
        clusteredArray.insert(i, cluster)

    # print(arrayofId, clusteredArray)
    return arrayofId, clusteredArray


arrayofId, clusteredArray = extractIDArray(number_of_clusters, datawithId[:, 1:], X)

############################################################################

arrayofMeta = []
dictionary = {
    "age_group": "",
    "gender": ""
}

for sub_arr in arrayofId:
    length = len(sub_arr)
    inner_arr = np.array([])

    for elem in sub_arr:
        mask = full_data[:, 2] == elem
        res = full_data[mask, [-1, -2]]
        inner_arr = np.append(inner_arr, res)

    inner_arr = inner_arr.reshape(-1, 2)
    # print(inner_arr)
    unique_age, counts_age = np.unique(inner_arr[:, 0], return_counts=True)
    unique_gender, counts_gender = np.unique(inner_arr[:, 1], return_counts=True)

    age_desc_arr = np.asarray((unique_age, counts_age)).T
    gender_desc_arr = np.asarray((unique_gender, counts_gender)).T
    appended_gender_age = np.append(age_desc_arr, gender_desc_arr, axis=0)
    arrayofMeta.append(appended_gender_age)

############################################################################
# for normalizing clustered data

clusteredArrayCopy = np.copy(clusteredArray)

normalizedClusteredArray = []

# for i in range(0,number_of_clusters):
#   normalizedClusteredArray.append(getNorm(clusteredArrayCopy[i]))

############################################################################

normalizedClusterCenters = []


def getNorm1D(arr):
    max_value = np.max(arr)
    min_value = np.min(arr)

    for index in range(0, len(arr)):
        arr[index] = (arr[index] - min_value) / (max_value - min_value)
        arr[index] = round(arr[index], 2)

    return arr


# for i in range(number_of_clusters):
#   normalizedClusterCenters.append(getNorm1D(a.cluster_centers_[i].ravel()))

############################################################################

# normalizedClusteredArray = sorted(normalizedClusteredArray, key=lambda a: len(a), reverse=True)
def key_func(arr):
    sum = 0
    for pair_arr in arr:
        if pair_arr[0] == 'M' or pair_arr[0] == 'F':
            sum += int(pair_arr[1])
    return sum


# arrayofMeta = sorted(arrayofMeta, key=key_func, reverse=True)

############################################################################
# Ranks in clusters
arrayOfClusterRanksLocal = []
arrayOfClusterRanksKona = []

medianRankForClustersLocal = []
medianRankForClustersKona = []

for idArray in arrayofId:
    # print(idArray)
    LocalRankarray = []
    KonaRankArray = []
    for ID in idArray:
        for row in full_data:
            # if ID == row[1].astype(int):
            if ID == row[2]:
                LocalRankarray.append(row[0].astype(int))
                KonaRankArray.append(row[1].astype(int))
    arrayOfClusterRanksLocal.append(LocalRankarray)
    arrayOfClusterRanksKona.append(KonaRankArray)

for cluster in arrayOfClusterRanksLocal:
    medianRankForClustersLocal.append(np.median(cluster))

for cluster in arrayOfClusterRanksKona:
    medianRankForClustersKona.append(np.median(cluster))

#####################################################
#############################################################
# Percentages

MaleNumber = 0
FemaleNumber = 0
percentOfMale = 0
percentOfFemale = 0

for index, meta in enumerate(arrayofMeta):
    for part in meta:
        if (part[0] == 'F'):
            FemaleNumber = FemaleNumber + meta[len(meta) - 2][1].astype(int)
        if (part[0] == 'M'):
            MaleNumber = MaleNumber + meta[len(meta) - 1][1].astype(int)

arrayOfPercentage = []

for index, meta in enumerate(arrayofMeta):
    array = []

    for part in meta:
        if (part[0] == 'F'):
            percentOfFemale = (meta[len(meta) - 2][1].astype(int)) / FemaleNumber
            female = []
            female.append('F')
            female.append(percentOfFemale * 100)
            array.append(female)
        if (part[0] == 'M'):
            male = []
            percentOfMale = (meta[len(meta) - 1][1].astype(int)) / MaleNumber
            male.append('M')
            male.append(percentOfMale * 100)
            array.append(male)

    arrayOfPercentage.append(array)
##############################################################################

full_Id_List = []
for array in arrayofId:
    for id in array:
        full_Id_List.append(id)

inner_array = np.array([])
for ID in full_Id_List:
    maskValue = full_data[:, 2] == ID
    resValue = full_data[maskValue, [-1, -1]]
    inner_array = np.append(inner_array, resValue)

inner_Value = inner_array.reshape(-1, 2)
unique_ages, counts_ages = np.unique(inner_Value[:, -2], return_counts=True)
# print(unique_ages,counts_ages)


#######################################################################
percentageOfAgeGroups = []
for age, count in zip(unique_ages, counts_ages):
    array = []
    for cluster in arrayofMeta:
        for inner in cluster:
            if inner[0] == age:
                result = str(round((inner[1].astype(int) / count) * 100, 2)) + '%'
                array.append(result)
    percentageOfAgeGroups.append(array)

###############################################################
####silhouette####

silhouette_score(X_bis, b, metric=metricOfClustering)

#################################################################

# print(arrayofId)
###############################
# Making CSV of cluster ID, Medians, Means
arrayOfClusters_Id_Median_Mean = []

medians = []
means = []

for index, idsOfCluster in enumerate(arrayofId):
    # print(idsOfCluster)
    array = []
    for ID in idsOfCluster:
        for row in copyOfFull_data:
            # if ID == row[1].astype(int):
            if ID == row[2]:
                # print(row[3:23])
                array = np.concatenate([array, row[3:23]])
    # print(array,'len',len(array))
    array = array.astype(float)
    medians.append(np.median(array))
    means.append(round(np.mean(array), 3))

# print('Medians : ', medians)
# print('Means : ', means)

####################################################################

newDataSet = []
for index, idsOfCluster in enumerate(arrayofId):
    # print(idsOfCluster)
    array = []
    for ID in idsOfCluster:
        for i, row in enumerate(copyOfFull_data):
            arr = []
            if ID == row[2]:
                # print(row[3:23])
                # arr = row
                arr = np.append(arr, row)
                arr = np.append(arr, 'https://www.strava.com/athletes/' + ID)
                arr = np.append(arr, index + 1)
                arr = np.append(arr, medians[index])
                arr = np.append(arr, means[index])
                # arr.append(index+1)
                # arr.append(medians[index])
                # arr.append(means[index])
                newDataSet.append(arr)
# print(newDataSet,len(newDataSet))


#################################################################
##PLOTING


##########################################
##Creating CSV

length = len(newDataSet[0])
print(length)
headers = ['Week ' + str(i) for i in range(1, length - 8)]
headers.insert(0, 'Local Rank')
headers.insert(1, 'Kona Rank')
headers.insert(2, 'id')
headers.insert(length + 3, 'Gender')
headers.insert(length + 4, 'Age_Group')
headers.insert(length + 5, 'Link to Strava')
headers.insert(length + 6, 'Cluster #')
headers.insert(length + 7, 'Cluster Median')
headers.insert(length + 8, 'Cluster Mean')


ref = 'D:\Spring_2020\data-scraping\Kona_New_Screenshots\For_Vaagn&Satenik\CSV\Medians'
data_pd = pd.DataFrame(newDataSet, columns=headers)
data_pd.to_csv(ref + '/' + title + "_cluster_" + str(number_of_clusters) + '.csv')

