import numpy as np
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from sklearn.preprocessing import Normalizer

initialData = pd.read_csv('week_time_All_Kona.csv')  # load data set

x = np.array(initialData)

minVal = 7
maxVal = 40
data = []

for row in range(0, len(x)):
    length = len(x[0])
    inner_data = [int(x[row, 1])]  # adding id to the row

    gender = x[row, length-2]  # adding gender to the row
    age = x[row, length-1]  # adding age to the row

    for col in range(2, 22):
        hours = x[row, col]
        if int(hours) in range(minVal, maxVal):
            inner_data.append(hours)
        else:
            if int(hours) < minVal:
                hours = minVal
                inner_data.append(hours)
            elif int(hours) >= maxVal:
                hours = maxVal
                inner_data.append(hours)

    inner_data.append(str(gender))  # adding gender to the row
    inner_data.append(str(age))  # adding age to the row

    data.append(inner_data)

print(data)

def getNorm(arr):
    for row in range(0, len(arr)):
        maximumVal = 0.0
        minimumVal = 0.0
        for column in range(1, len(arr[0])-2):

            if column == 1:
                minimumVal = arr[row][column]
            if float(arr[row][column]) >= maximumVal:
                maximumVal = arr[row][column]
            if float(arr[row][column]) < minimumVal:
                minimumVal = arr[row][column]

        for column in range(1, len(arr[0])-2):

            if maximumVal == minimumVal:
                maximumVal = 7.1
                minimumVal = 7.0

                print("Stex em", row, column, arr[row])

            arr[row][column] = (arr[row][column] - minimumVal) / (maximumVal - minimumVal)
            arr[row][column] = round(arr[row][column], 2)

    return arr

dataNormalized = getNorm(data)

# print(dataNormalized)

length = len(dataNormalized[0])

headers = ['Week' + str(i) for i in range(1, length-2)]
headers.insert(0, 'id')
headers.insert(length - 2, 'Gender')
headers.insert(length - 1, 'Age_Group')

data_pd = pd.DataFrame(dataNormalized, columns=headers)

data_pd.to_csv('Kona_groups/Kona_Male_Min_Max_Norm.csv')
