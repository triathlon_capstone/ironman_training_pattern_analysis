import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd

headers = np.array([i for i in range(1, 21)])
colors = cm.rainbow(np.linspace(0, 1, 4))

data = pd.read_csv('New_DataSets/Kona.csv')  # load data set


X = headers.reshape(-1, 1)
pre_y = data.iloc[:, 3:23].values
arrray = []
for i in range(len(pre_y[0])):
    arrray.append(np.average(pre_y[:, i]))
Y = np.array(arrray).reshape(-1, 20)
poly = PolynomialFeatures(degree=1)
for i, c in zip(Y, colors):


    plt.scatter(X, i, color=c)
    plt.plot(X, i, color="red")

    plt.title('Kona')
    plt.xlabel('Weeks')
    plt.ylabel('Time Amount')

plt.show()
