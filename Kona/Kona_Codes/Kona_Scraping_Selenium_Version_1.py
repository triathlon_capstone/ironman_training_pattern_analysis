from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import time
import numpy as np
import pandas as pd

# driver = webdriver.Chrome() # Driver for some computers

driver = webdriver.Chrome(executable_path=r'C:\webdrivers\chromedriver.exe')  # Driver for other computers

login_url = "https://www.strava.com/login"

driver.get(login_url)
driver.find_element_by_id("email").send_keys("nelson_mkrtchyan@edu.aua.am")
driver.find_element_by_id("password").send_keys("Capstone2020")
driver.find_element_by_id("login-button").click()
time.sleep(2)
request_cookies_browser = driver.get_cookies()

hrefs = []


def getHrefs():
    container = driver.find_element_by_id("results")
    links = container.find_elements_by_tag_name('a')
    for link in links:
        hrefs.append(link.get_attribute('href'))
    return hrefs


def filt(hrefs):
    filteredElems = []
    for el in hrefs:
        if "athletes" in el:
            filteredElems.append(el)
    return filteredElems


max_page_number = 36  # This range depends on the participants number in the list
numbers = np.array(range(1, max_page_number))

for number in numbers:
    table_url = "https://www.strava.com/segments/13650960?filter=overall&per_page=100&page=" + str(number)
    print(table_url)

    driver.get(table_url)
    time.sleep(2)
    hrefs = getHrefs()

filtered = filt(hrefs)


def getCols():
    data = []
    for path in filtered:
        athlete_url = path + '#interval_type?chart_type=hours&interval_type=week&interval=202009&year_offset=0'
        driver.get(athlete_url)
        time.sleep(7)
        min = 7
        max = 40
        try:
            elem = driver.find_element_by_id("interval-graph-columns")
            elemForRation = elem.find_elements_by_class_name("y-axis")[0].find_elements_by_class_name("label")[2]
            scaleTime = elemForRation.text
            scalePixel = elemForRation.get_attribute("style").split(':')[1].split('px')[0]
            ratio = float(scaleTime) / float(scalePixel)
            cont = elem.find_elements_by_tag_name("ul")[1]
            columns = elem.find_elements_by_tag_name("li")
            konaColumn = columns.index(cont.find_element_by_id("interval-201941"))
            id = path.split('/')[-1]
            inner_data = [id]
            for i in range(konaColumn - 20, konaColumn):
                height = columns[i].find_elements_by_tag_name('a')[0].find_elements_by_tag_name('div')[0].get_attribute(
                    "style").split(':')[1].split('px')[0]
                hours = round(int(height) * ratio, 2)
                if int(hours) in range(min, max):
                    inner_data.append(hours)
                else:
                    if int(hours) < min:
                        hours = min
                        inner_data.append(hours)
                    elif int(hours) >= max:
                        hours = max
                        inner_data.append(hours)
            data.append(inner_data)

        except Exception:
            pass
    return data


def getNorm(arr):
    for row in range(0, len(arr)):
        max = 0.0
        min = 0.0
        for column in range(1, len(arr[0])):
            if column == 1:
                min = arr[row][column]
            if arr[row][column] > max:
                max = arr[row][column]
            if arr[row][column] < min:
                min = arr[row][column]

        for column in range(1, len(arr[0])):
            arr[row][column] = (arr[row][column] - min) / (max - min)
            arr[row][column] = round(arr[row][column], 2)
    return arr


data = getCols()
dataNorm = getNorm(data)
print(dataNorm)
headers = ['Week' + str(i) for i in range(1, len(dataNorm[0]))]
headers.insert(0, 'id')
data_pd = pd.DataFrame(dataNorm, columns=headers)

data_pd.to_csv('week_time_(31-35_pages)_Kona_seleniumTime.csv')
