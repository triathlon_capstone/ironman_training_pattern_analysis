import json
from lxml import html
import requests
import lxml.html as lh
import urllib3
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd

session_requests = requests.session()
login_url = "https://www.strava.com/login"
result = session_requests.get(login_url)
tree = html.fromstring(result.text)
authenticity_token = list(set(tree.xpath("//input[@name='authenticity_token']/@value")))[0]
payload = {'email': 'nelson_mkrtchyan@edu.aua.am', 'password': 'Capstone2020', 'authenticity_token': authenticity_token}
result = session_requests.post(
    "https://www.strava.com/session",
    data=payload,
    headers=dict(referer=login_url)
)

url = "https://www.strava.com/segments/13650960?filter=overall&per_page=50"


response = session_requests.get(url)
tree = html.fromstring(response.content)
soup = BeautifulSoup(response.content, 'html.parser')


# headers=dict(
#         secFetchSite='same-origin',
#         secFetchMode='cors',
#         xRequestedWith='XMLHttpRequest',
#         referer='https://www.strava.com/athletes/5722',
#         xCsrfToken='pW1Ja41mZxghEgv6CVmRx6JzdDkl0AbKU61zvtRoJ7OEf8Zp3eforj2fUZ4WgvP19jMb8YO/a6F5KrebnD8IbQ==',
#         cookie='_ga=GA1.2.1939315871.1580639022; sp=4c81edd0-db0f-4559-8096-d46a1eb210ae; _fbp=fb.1.1580639023937.1015026485; _strava_cookie_banner=true; ajs_user_id=50732627; ajs_anonymous_id=%2295b90fc8-0eba-4d9e-b696-d45b8336ce7f%22; ajs_group_id=null; _sp_ses.047d=*; _gid=GA1.2.93118080.1582974461; _strava4_session=1bs769bn8akht4dchri7mjobgp4v51eb; _sp_id.047d=63d67e0d-f458-4448-9c0d-95823ec81c0d.1582398292.7.1582974908.1582465952.5493ad33-82b0-49a5-ab17-4558e80d1bf4')


def getHrefs():
    hrefs = []
    container = tree.xpath("//div[@id='results']")[0]
    links = container.xpath('.//a/@href')
    for link in links:
        hrefs.append(link)
    return hrefs




def filt(hrefs):
    filteredElems = []
    for el in hrefs:
        if "athletes" in el:
            filteredElems.append(el)
    return filteredElems


hrefs = getHrefs()
filtered = filt(hrefs)


# def getMins():
#     data = []
#     for path in filtered:
#         url = 'https://www.strava.com/' + path
#         res = session_requests.get(url)
#         soup = BeautifulSoup(res.content, 'html.parser')
#         try:
#             mins = soup.find("ul", {"id":"totals"}).findChildren()[3].text
#             mins  = float(mins.split('h')[0]) * 60 + float(mins.split('h')[1].split('m')[0])
#             data.append([1,mins])
#         except Exception:
#             pass
#     return data




def getCols():   
    data=[]
    for path in filtered:
        url = 'https://www.strava.com/' + path + '#interval_type?chart_type=hours&interval_type=week&interval=202009&year_offset=0&_=1584265819756'
        # params={'chart_type':'hours', 'interval_type':'week', 'interval':'202009', 'year_offset':'0', '_':'1583140406668'}
        headers = dict(XRequestedWith = 'XMLHttpRequest')
        res = session_requests.get(url, headers=headers)
        soup = BeautifulSoup(res.content, 'html.parser')
        try:    
            cont = soup.find("div", {"id":"interval-graph-columns"}).findChildren("ul")[1]  
            columns = cont.findChildren('li')  
            konaColumn = columns.index(cont.findChildren("li", {"id":"interval-201941"})[0])
            scale = soup.find("div", {"id":"interval-graph-columns"}).findChildren("ul")[0]
            print(scale.findChildren('li')[2].text)
            ratio = float(scale.findChildren('li')[2].text)/float(scale.findChildren('li')[2]['style'].split(':')[1].split('px')[0])
            id = path.split('/')[-1]   
            inner_data = [id]    
            for i in range(konaColumn-20,konaColumn):
                height = columns[i].findChildren('a')[0].findChildren('div')[0]['style'].split(':')[1].split('px')[0]
                hours = round(int(height)*ratio,2)
                inner_data.append(hours)
            data.append(inner_data)
                      
        except Exception:
            pass
    return data
    

data = getCols()
print(data)
headers = ['Week' + str(i) for i in range(1, len(data[0]))]
headers.insert(0, 'id')
data_pd = pd.DataFrame(data, columns=headers)

data_pd.to_csv('week_time_50Cs.csv')


# print(data[0].get("km"))      


# for activities
# hrefsForAthletes = []
# for item in filtered:
#     path = "https://www.strava.com" + item 
#     res = session_requests.get(path)
#     soup = BeautifulSoup(res.content, 'html.parser')   
# finded = soup.find_all('a')
# for link in finded:
#     if("athletes" in link.get('href')):
#         hrefsForAthletes.append(link.get('href'))


# useful scraping methods
# seg = tree.xpath('//div[@id="segment-leaderboard"]')[0]
# row = seg.xpath('.//div')[0]
# d1 = seg.xpath('.//div')[1]
# table = seg.xpath('.//table')[1]
# tr0 = seg.xpath('.//tr')[0]
# td1 = seg.xpath('.//td/')[1]
# strong = tree.xpath('.//strong')
# results = tree.xpath('//div[@id="results"]')
# board = tree.xpath('.//td[@class="track-click"]')[0]


# [['/athletes/7134796', 8.4, 11.6, 33.2, 26.8, 14.8, 18.4, 17.6, 30.8, 27.6, 25.6, 16.0, 16.0, 38.8, 8.4, 33.2, 12.8, 20.8, 3.2, 19.6, 17.2, 34.8, 30.8, 2.8, 8.4, 21.2, 20.8, 19.6, 23.2, 36.8, 24.4, 13.2, 6.8, 22.0, 4.8, 8.0, 12.4, 9.6, 11.6, 12.0, 11.6, 8.8, 10.0, 9.2, 14.0, 13.2, 11.2, 9.6, 11.6, 8.0, 14.4, 12.8, 17.6, 8.4, 2.0], ['/athletes/10736261', 35.6, 19.6, 26.4, 32.4, 14.4, 19.6, 30.0, 39.6, 28.4, 16.8, 17.2, 19.2, 26.4, 29.6, 20.0, 26.4, 33.2, 21.2, 30.8, 25.6, 16.8, 32.4, 21.2, 34.8, 18.8, 24.8, 16.8, 36.0, 8.8, 14.4, 24.8, 28.4, 24.4, 30.8, 24.0, 14.4, 19.6, 30.4, 28.8, 19.2, 24.8, 17.6, 20.4, 24.0, 22.0, 28.8, 11.6, 23.6, 28.4, 23.2, 21.2, 19.2, 12.8], ['/athletes/1682310', 19.6, 12.4, 21.6, 28.8, 18.8, 12.0, 12.0, 21.2, 22.0, 33.2, 31.6, 25.2, 10.4, 6.8, 14.8, 29.6, 30.4, 16.0, 28.8, 21.2, 20.8, 18.0, 33.2, 29.6, 33.2, 19.6, 30.0, 23.2, 38.8, 38.8, 16.4, 33.2, 30.8, 5.2, 32.0, 24.8, 30.4, 26.0, 34.4, 30.8, 6.8, 13.2, 10.4, 11.2, 29.2, 19.2, 6.0, 21.2, 22.4, 34.4, 28.4, 20.8, 22.0, 6.0], ['/athletes/120662', 15.2, 12.8, 8.4, 12.0, 9.6, 14.8, 3.6, 12.4, 12.4, 7.6, 12.0, 23.6, 6.0, 20.0, 13.6, 23.6, 8.0, 6.0, 18.8, 15.2, 10.8, 4.0, 8.4, 11.6, 18.0, 18.4, 6.8, 15.2, 8.0, 16.4, 6.4, 15.2, 15.2, 2.8, 1.6, 8.0, 23.6, 19.6, 13.6, 10.4, 9.6, 9.2, 10.0, 10.8, 19.6, 13.2, 16.4, 8.0, 20.8, 38.0, 31.6, 17.6, 22.0, 4.4], ['/athletes/121448', 18.0, 14.8, 9.6, 10.8, 16.8, 14.4, 16.0, 32.8, 8.8, 18.0, 18.8, 16.0, 30.8, 11.6, 19.2, 29.2, 17.2, 12.4, 38.0, 36.8, 30.4, 32.0, 31.6, 14.0, 21.6, 22.4, 20.4, 20.0, 5.2, 26.8, 5.6, 12.4, 11.6, 6.8, 6.8, 10.8, 15.2, 9.6, 13.2, 14.4, 19.2, 12.8, 14.4, 12.4, 25.6, 18.0, 28.8, 22.4, 26.0, 25.2, 25.2, 5.6, 30.4, 1.2], ['/athletes/10342404', 12.4, 12.8, 10.4, 38.0, 28.4, 17.6, 15.6, 28.4, 12.0, 12.0, 18.8, 14.0, 11.6, 12.4, 13.2, 15.2, 13.6, 30.0, 16.0, 16.8, 14.0, 16.0, 20.0, 18.8, 18.0, 13.6, 14.0, 15.2, 18.8, 12.0, 2.0, 8.4, 8.4, 11.2, 12.0, 13.2, 14.8, 12.0, 11.6, 12.8, 12.8, 10.4, 14.4, 15.6, 13.6, 13.6, 14.0, 14.8, 15.6, 14.0, 13.6, 13.6, 9.2, 2.4], ['/athletes/12485408', 20.8, 16.8, 36.8, 38.8, 34.0, 33.6, 33.6, 34.4, 27.2, 36.4, 34.4, 11.2, 8.0, 6.0, 8.0, 5.2, 15.2, 18.8, 8.0, 20.8, 28.4, 23.2, 28.4, 17.6, 34.4, 38.4, 26.8, 31.2, 33.2, 33.2, 18.0, 22.8, 5.6, 8.4, 16.8, 16.8, 22.8, 14.8, 15.2, 16.0, 15.2, 10.4, 2.8, 17.6, 17.2, 13.6, 25.2, 18.8, 10.0, 3.2, 4.8, 4.0, 3.6]]
