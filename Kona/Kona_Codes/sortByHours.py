import numpy as np
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from sklearn.preprocessing import Normalizer

initialData = pd.read_csv('Kona_groups/Kona_Female_35_69.csv')  # load data set

x = np.array(initialData)

data1 = []
data2 = []

for row in range(0, len(x)):
    sum = round(np.sum(x[row][2:22]), 2)
    if sum < 150:
        data1.append(x[row][1:len(x)])
    else:
        data2.append(x[row][1:len(x)])

length = len(data1[0])

headers = ['Week' + str(i) for i in range(1, length - 2)]
headers.insert(0, 'id')
headers.insert(length - 2, 'Gender')
headers.insert(length - 1, 'Age_Group')

data_pd = pd.DataFrame(data1, columns=headers)
data_pd.to_csv('Kona_groups/Kona_Female_35_69_0_150hours.csv')

data_pd = pd.DataFrame(data2, columns=headers)
data_pd.to_csv('Kona_groups/Kona_Female_35_69_150+hours.csv')
