import numpy as np
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from sklearn.preprocessing import Normalizer

gender = "Female"
ageGroup = "20_24"

# initialData = pd.read_csv('New_DataSets/Kona.csv')  # load data set
initialData = pd.read_csv('New_DataSets/Kona' + '_' + gender + '_' + ageGroup + '.csv')  # load data set

x = np.array(initialData)

data1 = []
data2 = []

datawithId = x[:, 2:23]
data = datawithId[:, 1:23]
############################################################################
# Transfoming data

dataX = data.astype(float)
sz = x.shape[1]
arr_of_ind = []

for i, row in enumerate(dataX):
    if (sum(row < 7) + sum(row > 40)) > 10:
        arr_of_ind.append(i)
x = np.delete(x, arr_of_ind, axis=0)

bigArray = np.array([])
for array in x:
    bigArray = np.concatenate([bigArray, array[3:23]])

median = np.median(bigArray)

for row in range(0, len(x)):
    medianRow = np.median(x[row][3:23])
    if medianRow < median:
        data1.append(x[row][1:len(x)])
    else:
        data2.append(x[row][1:len(x)])

# headers = ['Week' + str(i) for i in range(1, length - 2)]
# headers.insert(0, 'id')


length = len(data[0])
headers = ['Week ' + str(i) for i in range(1, length + 1)]
headers.insert(0, 'Rank')
headers.insert(1, 'id')
headers.insert(length + 2, 'Gender')
headers.insert(length + 3, 'Age_Group')

# data_pd = pd.DataFrame(data1, columns=headers)
# data_pd.to_csv('Kona_groups/Kona_MaleFemale_0_34_Smaller_Median.csv')
#
# data_pd = pd.DataFrame(data2, columns=headers)
# data_pd.to_csv('Kona_groups/Kona_MaleFemale_0_34_Bigger_Median.csv')


# print(data1)
#
data_pd = pd.DataFrame(data1, columns=headers)
data_pd.to_csv('New_DataSets/Medians/Kona' + '_' + gender + '_' + ageGroup + '_Smaller_Median(' + str(median) + ').csv')

data_pd = pd.DataFrame(data2, columns=headers)
data_pd.to_csv('New_DataSets/Medians/Kona' + '_' + gender + '_' + ageGroup + '_Bigger_Median(' + str(median) + ').csv')
